" SPDX-License-Identifier: GPL-3.0-or-later

" Check if the user uses at least Vim 8.0 or NeoVim
if version < 800
	echo "Vim 8.0 or higher is required for the config file to work! Please upgrade!"
	quit
endif

" Reload the vimrc as soon as it gets changed.
autocmd	 BufWritePost .vimrc source $MYVIMRC
autocmd	 BufWritePost init.vim source $MYVIMRC

" Install Plugins with plug.vim
call plug#begin()
Plug 'nvim-lua/plenary.nvim'           " Telescope dependency
Plug 'nvim-treesitter/nvim-treesitter' " Another Telescope dependency
Plug 'tpope/vim-sensible'              " Sensible defaults
Plug 'nvim-telescope/telescope.nvim', { 'tag': '0.1.0' }
                                       " Nice fuzzy finder
Plug 'sheerun/vim-polyglot'            " Language pack
Plug 'dylanaraps/wal.vim'              " wal colorscheme for Pywal integration (!=pywal.nvim)
call plug#end()

filetype on
filetype plugin indent on
syntax on

set number
set numberwidth=4
set relativenumber

set fileformat=unix
set encoding=utf-8
set fileencoding=utf-8

set tabstop=8
set shiftwidth=8
set softtabstop=8
set noexpandtab

set listchars+=eol:¬
set listchars+=trail:•
set list

nmap <Tab> >>
imap <S-Tab> <Esc><<i
nmap <S-tab> <<

colorscheme wal

autocmd BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif

" ~> https://coderwall.com/p/if9mda/automatically-set-paste-mode-in-vim-when-pasting-in-insert-mode
let &t_SI .= "\<Esc>[?2004h"
let &t_EI .= "\<Esc>[?2004l"

function! XTermPasteBegin()
    set pastetoggle=<Esc>[201~
    set paste
    return ""
endfunction
inoremap <special> <expr> <Esc>[200~ XTermPasteBegin()

nnoremap <C-f> :Telescope find_files theme=dropdown<CR>
nnoremap <C-B> :Telescope buffers theme=dropdown<CR>
nnoremap <C-o> :Telescope oldfiles theme=dropdown<CR>

set completeopt=longest,menuone,menu
set omnifunc=syntaxcomplete#Complete

imap <C-o> <C-x><C-o>
imap <C-f> <C-x><C-f>
imap <C-t> <C-x><C-]>

nnoremap <C-l> :tabn<CR>
nnoremap <C-h> :tabp<CR>
nnoremap <C-t> :tabnew<CR>

set mouse=
